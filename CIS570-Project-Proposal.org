*Analysis of Compilation Complexity of CISC and RISC Architectures*

Final Project Proposal

CIS 570, Fall 2020

*Grady Landers: *[[mailto:glanders@umassd.edu][*glanders@umassd.edu*]]

*Andrew Singley: *[[mailto:asingley@umassd.edu][*asingley@umassd.edu*]]

Description:

The hypothesis of this project claims compilation of high-level
languages targeting Reduced Instruction Set Computer (RISC)
architectures has higher complexity than Complex Instruction Set
Computer (CISC) architectures because more Instruction Set Architecture
(ISA) instructions need to be compiled to compensate for the reduced
instruction set size. This project will attempt to assert the validity
of this hypothesis by cross compiling non-trivial programs targeting a
sample set of CISC and RISC ISAs. A variety of compilers, optimization
settings, and program types.

Very little research analyzing the complexity of compiling for CISC and
RISC has been discovered from a couple hours of searching. Senior
Software Engineer at Apple, Rustam Muginov stated on Quora in response
to the question, “How do X86/CISC compilers compare with ARM/RISC
compilers in performance and features?” “Compiler performance means
almost nothing, it matters very little if program is compiled 30 seconds
or 60 seconds.” This quote demonstrates the indifference with prejudice
of compilation complexity of many software developers. It is taught by
many universities that compilation time should not matter if runtime
performance can be improved. This project goes against the trend by
intentionally determining compilation time and space complexity because
the hardware is the primary concern instead of the software or end user
experience.

Some test programs will be compiled to Low Level Virtual Machine (LLVM)
Intermediate Representation (IR) and cross compiled to machine code of
CISC architectures such as x86 variants and other ISAs if available such
as Motorola 68k, Z80, etc. RISC ISAs such as ARM, RISC-V, and PowerPC
may also be selected. Program source code that compiles into SIMD
instructions are expected to have the highest variance because those
will be simplified the biggest difference between RISC and CISC
architectures. Unfortunate for this hypothesis, Even ARM A and R series
CPUs now have a Single Input Multiple Data (SIMD) instruction set,
called AMD NEON, to compete against the CISC x86 Streaming SIMD
Extensions (SSE). A lot of care and additional thought is needed to
accurately measure variance between CISC and RISC architectures in with
enough equivalence to draw conclusions from its findings.

CISC architecture, as the name implies, utilizes a larger set of
instructions within the ISA, each with more unique use cases. This
method favors hardware level advancements because those complex
instructions are carried out by similarly complex functionality built
into the processor itself. The goal of CISC is to reduce the number of
instructions within a high-level program and pass those savings to the
memory, which was once scarce but is now used in abundance by complex
simulations. This benefit comes at the cost of cycles per instruction,
but the average consumer processor has four to eight cores to make up
for this.

RISC architecture, on the other hand, minimizes the number of
instructions in the ISA to keep hardware design simple. This method
favors software advancements by offering a small but effective set of
instructions that can be combined to perform almost any task you could
think of, rather than having many predefined instructions with no scope
beyond what is predefined. RISC processors also have the benefit of
requiring less energy to run efficiently, making the architecture an
obvious choice for mobile devices, which have grown increasingly popular
over the years.

What we aim to discover through this research is which architecture
lends itself to faster compilation of high-level languages. The point of
the ISA, after all, is to facilitate communication between the user and
the hardware responsible for doing all the hard work. When high-level
languages, such as C and C++ for example, are written by programmers, no
knowledge of the underlying architecture is required. Compilers for the
high-level languages take that code and convert it into instructions the
ISA can understand, assembly code. The number of instructions in the ISA
dictates the number of assembly instructions required to simulate a
single line in a high-level language. The more unique ISA instructions
available, the more likely that fewer instructions will simulate just
one high-level instruction. When modern applications span dozens of
thousands of lines of code, compile time can seriously hinder the
development process. Based on this fact, we believe compiling a high
level language to CISC architecture will prove to be speedier than
compiling the same program to a RISC architecture, and intend to prove
it through the use of LLVM and IR.

References:

[[https://developers.redhat.com/blog/2019/05/15/2-tips-to-make-your-c-projects-compile-3-times-faster/][*https://developers.redhat.com/blog/2019/05/15/2-tips-to-make-your-c-projects-compile-3-times-faster/*]]

[[https://www.watelectronics.com/what-is-risc-and-cisc-architecture/][*https://www.watelectronics.com/what-is-ri*<<anchor>><<anchor-1>>*sc-and-cisc-architecture/*]]

[[https://techdifferences.com/difference-between-risc-and-cisc.html][*https://techdifferences.com/difference-betw*<<anchor-2>><<anchor-3>>*een-*<<anchor-4>><<anchor-5>>*risc-and-cisc.html*]]

[[https://cs.stanford.edu/people/eroberts/courses/soco/projects/risc/risccisc/][*https://cs.stanford.edu/people/eroberts/c*<<anchor-6>><<anchor-7>>*ourses/soco/projects/risc/risccisc/*]]

[[https://cs.lmu.edu/~ray/notes/ir/][*https://cs.lmu.edu/~ray/notes/ir/*]]

[[https://www.quora.com/How-do-X86-CISC-compilers-compare-with-ARM-RISC-compilers-in-performance-and-features][*https://www.quora.com/How-do-X86-CISC-compilers-compare-with-ARM-RISC-compilers-in-performance-and-features*]]

[[https://archive.arstechnica.com/cpu/4q99/risc-cisc/rvc-5.html#Branch][*https://archive.arstechnica.com/cpu/4q99/risc-cisc/rvc-5.html#Branch*]]

[[http://www.borrett.id.au/computing/art-1991-06-02.htm][*http://www.borrett.id.au/computing/art-1991-06-02.htm*]]

[[https://www.researchgate.net/publication/2794870_Performance_from_Architecture_Comparing_a_RISC_and_a_CISC_with_Similar_Hardware_Organization][*https://www.researchgate.net/publication/2794870_Performance_from_Architecture_Comparing_a_RISC_and_a_CISC_with_Similar_Hardware_Organization*]]

[[https://en.wikipedia.org/wiki/Complex_instruction_set_computer][*https://en.wikipedia.org/wiki/Complex_instruction_set_computer*]]

[[http://www.cs.cornell.edu/courses/cs3410/2019sp/schedule/slides/09-isa-notes-bw.pdf][*http://www.cs.cornell.edu/courses/cs3410/2019sp/schedule/slides/09-isa-notes-bw.pdf*]]

[[http://www.mscs.mu.edu/~rge/cosc2200/homework-fall2013/Readings/RISC-CISC.pdf][*http://www.mscs.mu.edu/~rge/cosc2200/homework-fall2013/Readings/RISC-CISC.pdf*]]

[[https://www.cs.princeton.edu/courses/archive/spr03/cs320/notes/IR-trans1.pdf][*https://www.cs.princeton.edu/courses/archive/spr03/cs320/notes/IR-trans1.pdf*]]

[[http://www.cis.udel.edu/~pollock/471/project2spec.pdf][*http://www.cis.udel.edu/~pollock/471/project2spec.pdf*]]

[[http://llvm.org/pubs/2004-01-30-CGO-LLVM.pdf][*http://llvm.org/pubs/2004-01-30-CGO-LLVM.pdf*]]

[[https://llvm.org/pubs/2009-10-TereiThesis.pdf][*https://llvm.org/pubs/2009-10-TereiThesis.pdf*]]
